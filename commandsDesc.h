DEF (PUSH, 1, 4, {
    Elem_t tmp = *( Elem_t* )cmdBuf;
    cmdBuf += sizeof( Elem_t );
    cur += sizeof( Elem_t );
    stackPush( SystemStack, tmp * 10000 );
})

DEF (POP, 2, 0, {
    stackPop( SystemStack );
})

DEF (ADD, 3, 0, {
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    stackPush( SystemStack, first + second );
})

DEF (SUB, 4, 0, {
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    stackPush( SystemStack, second - first );
})

DEF (MULT, 5, 0, {
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    stackPush( SystemStack, (first/10000) * second );
})

DEF (DIV, 6, 0, {
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    //printf( "%d / %d = %d\n",  second, first, (int)( (double)(second) / (double)(first) * 10000));
    if( first != 0 )stackPush( SystemStack, (int)( (double)(second) / (double)(first) * 10000) );
})

DEF (MOD, 7, 0, {
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    if( second != 0 )stackPush( SystemStack, first % second );
})

DEF (END, 8, 0, {
    return;
})

DEF (DUMP, 9, 0, {
    STACK_DUMP( SystemStack, "LOOKING FROM CPU");
})

DEF (OUT, 10, 0, {
    printf( "Top SystemStack element was %.5lf\n", (double) (  (double)stackPop( SystemStack ) / (double)10000) );
})

DEF(PUSH_REG, 11, 2, {
    char type = *cmdBuf;
    cmdBuf++;
    cur++;
    stackPush( SystemStack, reg[type - 1] );
})

DEF(POP_REG, 12, 2, {
    char type = *cmdBuf;
    cmdBuf++;
    cur++;
    reg[type - 1] = stackPop( SystemStack );
})

DEF(JMP, 13, 1, {
    int dest = 0;
    dest = *(int*)cmdBuf;
    cmdBuf -= cur - dest;
    cur = dest;
})

DEF(JA, 14, 1, {
    int dest = 0;
    dest = *(int*)cmdBuf;
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    stackPush( SystemStack, second );
    stackPush( SystemStack, first );
    if( second < first ) {
        cmdBuf -= cur - dest;
        cur = dest;
    } else {
        cmdBuf += sizeof( int );
        cur += sizeof( int );
    }
})

DEF(JAE, 15, 1, {
    int dest = 0;
    dest = *(int*)cmdBuf;
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    stackPush( SystemStack, second );
    stackPush( SystemStack, first );
    if( second <= first ) {
        cmdBuf -= cur - dest;
        cur = dest;
    } else {
        cmdBuf += sizeof( int );
        cur += sizeof( int );
    }
})

DEF(JL, 16, 1, {
    int dest = 0;
    dest = *(int*)cmdBuf;
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    stackPush( SystemStack, second );
    stackPush( SystemStack, first );
    if( second > first ) {
        cmdBuf -= cur - dest;
        cur = dest;
    } else {
        cmdBuf += sizeof( int );
        cur += sizeof( int );
    }
})

DEF(JLE, 17, 1, {
    int dest = 0;
    dest = *(int*)cmdBuf;
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    stackPush( SystemStack, second );
    stackPush( SystemStack, first );
    if( second >= first ) {
        cmdBuf -= cur - dest;
        cur = dest;
    } else {
        cmdBuf += sizeof( int );
        cur += sizeof( int );
    }
})

DEF(JE, 18, 1, {
    int dest = 0;
    dest = *(int*)cmdBuf;
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    stackPush( SystemStack, second );
    stackPush( SystemStack, first );
    if( first == second ) {
        cmdBuf -= cur - dest;
        cur = dest;
    } else {
        cmdBuf += sizeof( int );
        cur += sizeof( int );
    }
})

DEF(JNE, 19, 1, {
    int dest = 0;
    dest = *(int*)cmdBuf;
    Elem_t first = stackPop( SystemStack );
    Elem_t second = stackPop( SystemStack );
    stackPush( SystemStack, second );
    stackPush( SystemStack, first );
    if( first != second ) {
        cmdBuf -= cur - dest;
        cur = dest;
    } else {
        cmdBuf += sizeof( int );
        cur += sizeof( int );
    }
})

DEF (TOP, 20, 0, {
    Elem_t tmp = stackPop( SystemStack );
    stackPush ( SystemStack, tmp );
    printf( "Top SystemStack element was: %.5lf\n", (double)(tmp / 10000) );
})

DEF (IN, 21, 0, {
    printf( "Print an integer number...\n");
    Elem_t tmp = 0;
    scanf( "%d", &tmp );
    stackPush( SystemStack, tmp * 10000 );
})

DEF(CALL, 22, 1, {
    int dest = *(int*)cmdBuf;
    int put = cur + sizeof(int);

    funcs[curFunc] = put;
    curFunc++;

    cmdBuf -= cur - dest;
    cur = dest;
})

DEF(RET, 23, 0, {
    int dest = 0;
    curFunc--;
    dest = funcs[curFunc];
    cmdBuf -= cur - dest;
    cur = dest;
})

DEF(PRINT, 24, 3, {
    int len = *(int*)cmdBuf;
    cmdBuf += sizeof(int);
    printf( "%.*s\n", len, cmdBuf );
    cmdBuf += len;
})

DEF(SQRT, 25, 0, {
    Elem_t top = stackPop( SystemStack );
    //printf( "sqrt(%d) = %d\n", top, (int) ( (sqrt((double)(top/10000))) * 10000) );
    stackPush( SystemStack, (int) ( (sqrt((double)(top/10000))) * 10000)  );
})


