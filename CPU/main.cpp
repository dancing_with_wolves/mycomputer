//CPU
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "Stack.h"


#define DEF(cmdName, code, argc, executableCode) CMD_##cmdName = code,

enum commands {
   #include "../commandsDesc.h"
};


int countFileSize( const char* fname )
{
    assert( fname );
    struct stat st;
    stat( fname, &st );
    return st.st_size;
}

int readFile( char *buf, char *fileName, int fileSize )
{
    assert( buf );
    assert( fileName );
    FILE *input = fopen( fileName, "r" );
    int ret = fread( buf, sizeof( char ), fileSize, input );
    fclose( input );
    return ret;
}


#define DEF(cmdName, code, argc, executableCode) \
    case CMD_##cmdName:\
        cur++;\
        cmdBuf++;\
        executableCode;\
        break;\


void execute( char *cmdBuf, Stack_t *SystemStack, Stack_t *CallStack, int finish )
{
    assert( SystemStack );
    assert( CallStack );
    assert( cmdBuf );

    int cur = 0;
    Elem_t reg[5] = {};
    int funcs[128] = {};
    int curFunc = 0;
    char labels[64] = "";

    while( cur < finish ){
        switch ( *cmdBuf ){
            #include "../commandsDesc.h"
        }
    }
}


int main( int argc, char **argv ) {
    printf( "\n\n");
    char *inputCommandFileName = "";
    if( argc > 1 ) inputCommandFileName = argv[1]; else inputCommandFileName = "../commands.bin";

    int fileSize = countFileSize( inputCommandFileName );
    char *cmdBuf = (char*) calloc ( fileSize, sizeof(char) );

    int finish = readFile( cmdBuf, inputCommandFileName, fileSize );

    Stack_t SystemStack;
    Stack_t CallStack;

    STACK_INIT(&SystemStack);
    STACK_INIT(&CallStack);

    execute( cmdBuf, &SystemStack, &CallStack, finish );

    free( cmdBuf );
    //stackDestruct( &SystemStack );
    //stackDestruct( &CallStack );

    return 0;
}
