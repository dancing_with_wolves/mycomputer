//ASM

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <sys/stat.h>

#define DEF(cmdName, code, type, executableCode) CMD_##cmdName = code,
enum commands {
   #include "../commandsDesc.h"
};
#undef DEF

#define DEF(cmdName, code, type, executableCode) TYPE_##cmdName = type,
enum types {
   #include "../commandsDesc.h"
};


struct Label{
    int line;
    char *name;
};

int countFileSize( const char* fname )
{
    assert( fname );
    struct stat st;
    stat(fname, &st);
    return st.st_size;
}

int getCmdType( char *cmdName_ )
{
    #define DEF(cmdName, code, type, executableCode)\
    if( strcmp( #cmdName, cmdName_ ) == 0 ) return type;
    #include "../commandsDesc.h"
    return -1;
}

int searchLabelLine( Label *labels, char *labelName, int labelQty )
{
    for( int i = 0; i < labelQty; i++ ){
        if( strcmp( labels[i].name, labelName ) == 0 ) return labels[i].line;
    }
    printf( "Undefined label %s\n", labelName );
    return -1;
}

#define DEF(cmdName, code, type, executableCode) \
case CMD_##cmdName:\
    if( type == 4 ){ /*PUSH-like*/ \
        binCur += sizeof(int);\
    }\
    if( type == 3 ){ /*PRINT-like*/ \
        int skip = *(int*)&bin[binCur+1];\
        binCur += skip + sizeof(int);\
    }\
    if( type == 2 ){ /*PUSH_REG-like*/ \
        binCur += 1; /*ax -- 1 bytes*/ \
    }\
    if( type == 1 ){ /*JMP-like*/\
        flag = 0;\
        *(int*)&( bin[binCur + 1] ) = put;\
        binCur += sizeof(int);\
    }\
    binCur++;\
    break;\

void initLabels( char *bin, Label *labels, char *input, int labelQty, int inpSize, int binSize )
{
    assert( bin );
    assert( labels );
    assert( input );

    int inputCur = 0;
    int binCur = 0;
    char curCmd[128] = "";

    char labelName[128] = "";
    while( inputCur < inpSize ){
        //!!!!!
        sscanf( &(input[inputCur]), "%s", curCmd );
        inputCur += strlen( curCmd );
        //!!!!!
        if( getCmdType( curCmd ) == 1 ){ //if JMP-like
            while( input[inputCur] != ' ') inputCur++;

            sscanf( &input[inputCur], "%s", labelName );
            printf( "I put label %s\n", labelName );

            int put = searchLabelLine( labels, labelName, labelQty );
            int flag = 1;

            while( binCur < binSize ){
                switch( bin[binCur] ){
                    #include "../commandsDesc.h"
                }
                if( flag == 0 ){
                    flag = 1;
                    break;
                }
            }
        }
        inputCur++;
    }
    //ищем в инпуте джампы, прыгаем по outBuf и кладём на нужное место line соответствующего имени

}



#define DEF(cmdName, code, type, executableCode) if( strcmp( buf, #cmdName ) == 0 ){ \
    writeMe = CMD_##cmdName; \
    outBuf[i] = writeMe; \
    binSize++;\
    i++; \
    if( type == 4 ){\
        int tmp = 0;\
        sscanf( &input[counter], "%d", &tmp ); \
        /*printf("tmp = %d\n", tmp);*/\
        while( ( input[counter] != '\n' ) && ( input[counter] != ' ' ) ){\
            counter++;\
        }\
        counter++;\
        char *tmpPointer = (char*)&tmp; \
        for( int j = 0; j < sizeof(int); j++){ \
            outBuf[i] = *tmpPointer; \
            binSize++;\
            i++; \
            tmpPointer++; \
        } \
	}\
	if( type == 2 ){\
        sscanf( &input[counter], "%s", buf );\
        /*printf( "tmp2 = %s\n", buf );*/\
        counter += 3;/* это sizeof( tmp2 ) + 1*/\
        char put = -1;\
        if( strcmp( buf, "ax" ) == 0 ){\
            put = 1;\
        }\
        if( strcmp( buf, "bx" ) == 0 ){\
            put = 2;\
        }\
        if( strcmp( buf, "cx" ) == 0 ){\
            put = 3;\
        }\
        if( strcmp( buf, "dx" ) == 0 ){\
            put = 4;\
        }\
        if( strcmp( buf, "ex" ) == 0 ){\
            put = 5;\
        }\
        outBuf[i] = put; \
        binSize++;\
        i++; \
    } \
    if( type == 1 ){\
        sscanf( &input[counter], "%s", buf );\
        counter += strlen( buf ) + 1;\
        binSize += sizeof(int);\
        i += sizeof(int); \
    }\
    if( type == 3 ){\
        sscanf( &input[counter], "%s", buf );\
        /*printf( "PRINTED STRING IS %s\n", buf );*/\
        int len = strlen(buf);\
        counter += len + 1;\
        char *lenPointer = (char*)&len;\
        for( int j = 0; j < sizeof(int); j++){ \
            outBuf[i] = *lenPointer; \
            binSize++;\
            i++; \
            lenPointer++; \
        } \
        for( int j = 0; j < len; j++){ \
            outBuf[i] = buf[j]; \
            binSize++;\
            i++; \
        } \
    }\
}
void convert( char *input, FILE *output, int fileSize )
{
    assert( input );
    assert( output );

    printf( "\n\n\nSOURCE .ASM FILE IS:\n%s\n", input );

    char *outBuf = (char*) calloc ( fileSize, sizeof(char) );
    char buf[128] = "";
    int i = 0, k = 0; // i is for outBuf, k is for buf which is being read from &input[counter]

    Label labels[256] = {};
    int labelQty = 0;


    int counter = 0;
    int binSize = 0;

    while( counter < fileSize ){
        k = 0;
        if( input[counter] == ':' ){
            counter++;
            while( input[counter] != '\n' ){
                buf[k] = input[counter];
                k++;
                counter++;
            }
            while( ( input[counter] == ' ' ) || ( input[counter] == '\n' ) ) counter++;
            buf[k] = '\0';
            printf( "Detected label %s\n", buf);
            labels[labelQty].name = strdup( buf );
            labels[labelQty].line = i;
            labelQty++;
            k = 0;
        }
        while( ( input[counter] != ' ' ) && ( input[counter] != '\n' ) ){
            buf[k] = input[counter];
            k++;
            counter++;
        }
        while( ( input[counter] == ' ' ) || ( input[counter] == '\n' ) ) counter++;
        buf[k] = '\0';
        //printf("read buf = %s\n", buf);

        char writeMe = -126;
        #include "../commandsDesc.h"
    }
    initLabels( outBuf, labels, input, labelQty, fileSize, binSize );
    fwrite( outBuf, sizeof(char), i, output );
    free( outBuf );
}

int main( int argc, char **argv ) {
    char *inputName = nullptr, *outputName = nullptr;
    if( argc > 1 ) inputName = argv[1]; else inputName = "../commands.asm";
    if( argc > 2 ) outputName = argv[2]; else outputName = "../commands.bin";

    FILE *input = fopen( inputName, "r" );
    FILE *output = fopen( outputName, "wb" );

    int inputSize = countFileSize( inputName );
    char* inputStr = (char*) calloc ( inputSize, sizeof(char) );
    fread( inputStr, sizeof(char), inputSize, input );
    fclose( input );

    convert( inputStr, output, inputSize );

    fclose( output );
    free( inputStr );
    return 0;
}
