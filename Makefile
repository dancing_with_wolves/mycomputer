build:	CPU/cpu.o CPU/Stack.o ASM/asm.o 
	clang++ CPU/cpu.o CPU/Stack.o -o CPU/cpu.e
	clang++ ASM/asm.o -o ASM/asm.e
ASM/asm.o: ASM/main.cpp commandsDesc.h
	clang++ -c ASM/main.cpp -o ASM/asm.o
CPU/cpu.o: CPU/main.cpp commandsDesc.h
	clang++ -c CPU/main.cpp -o CPU/cpu.o
CPU/Stack.o: CPU/Stack.cpp
	clang++ -c CPU/Stack.cpp -o CPU/Stack.o
clean:
	rm ASM/*.o
	rm CPU/*.o
	rm *.bin
	rm *.log
run:	build commands.asm
	./ASM/asm.e commands.asm commands.bin
	./CPU/cpu.e commands.bin
